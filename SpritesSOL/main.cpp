#include <windows.h>
#include <string>
#include <cassert>
#include <d3d11.h>
#include <iomanip>
#include <vector>

#include "WindowUtils.h"
#include "D3DUtil.h"
#include "D3D.h"
#include "SimpleMath.h"
#include "SpriteFont.h"
#include "DDSTextureLoader.h"
#include "CommonStates.h"

#include <ctime>

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

#define VK_BACKSPACE 0x08

DirectX::SpriteFont *gpFont = nullptr;
DirectX::SpriteBatch *gpSpriteBatch = nullptr;
ID3D11ShaderResourceView *gpDigital = nullptr, *gpAnalog = nullptr, *gpClockHands = nullptr;
time_t now = time(0);
struct tm& t = *localtime(&now);
bool replay = false;
string ans;
struct gamedata
{
	int randNum;
	int randNum2, randNum3, correctAnswer;
	float Angles[3][2];
	float hourAngle, minAngle;
	string amORpm;
	int currentHrs, currentMins, currentSecs;
};


ID3D11ShaderResourceView *LoadTexture(MyD3D& d3d, const wstring& file)
{
	DDS_ALPHA_MODE alpha;
	ID3D11ShaderResourceView *pT = nullptr;
	if (CreateDDSTextureFromFile(&d3d.GetDevice(), file.c_str(), nullptr, &pT, 0, &alpha) != S_OK)
	{
		WDBOUT(L"Cannot load " << file << L"\n");
		assert(false);
		return false;
	}
	assert(pT);
	return pT;
}

void InitGame(MyD3D& d3d, gamedata& gdata)
{
	void seed();
	void getAngles(float Angles[3][2]);
	gpSpriteBatch = new SpriteBatch(&d3d.GetDeviceCtx());
	assert(gpSpriteBatch);

	gpFont = new SpriteFont(&d3d.GetDevice(), L"../bin/data/comicSansMS.spritefont");
	assert(gpFont);

	gpDigital = LoadTexture(d3d, L"../bin/data/digital_clock.dds");
	gpAnalog = LoadTexture(d3d, L"../bin/data/clock_face.dds");
	gpClockHands = LoadTexture(d3d, L"../bin/data/clock_hands.dds");
	seed();

	gdata.randNum = rand() % 3;
	
	getAngles(gdata.Angles);
}

void seed()
{
	srand(static_cast<unsigned>(time(0)));
}

void getAngles(float Angles[3][2])
{
	float randHrAngle, randMinAngle;
	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 1; j++)
		{
			randHrAngle = rand() % 13;
			randHrAngle = (randHrAngle * 30) * PI / 180;
			Angles[i][j] = randHrAngle;

			randMinAngle = rand() % 60;
			randMinAngle = (randMinAngle * 6) * PI / 180;
			Angles[i][j + 1] = randMinAngle;
		}
	}
}


//any memory or resources we made need releasing at the end
void ReleaseGame()
{
	delete gpFont;
	gpFont = nullptr;


	delete gpSpriteBatch;
	gpSpriteBatch = nullptr;

	ReleaseCOM(gpDigital);
	ReleaseCOM(gpAnalog);
	ReleaseCOM(gpClockHands);
}

//called over and over, use it to update game logic
void Update(float dTime, MyD3D& d3d, gamedata& gdata)
{
	string AMorPM(gamedata& gdata);
	now = time(0);
	t = *localtime(&now);
	gdata.currentHrs = t.tm_hour;
	gdata.currentMins = t.tm_min;
	gdata.currentSecs = t.tm_sec;

	gdata.hourAngle = ((gdata.currentHrs * 30) * PI / 180);
	gdata.minAngle = ((gdata.currentMins * 6) * PI / 180);

	gdata.Angles[2][0] = gdata.hourAngle;
	gdata.Angles[2][1] = gdata.minAngle;

	gdata.amORpm = AMorPM(gdata);
}

string AMorPM(gamedata& gdata)
{
	if (gdata.currentHrs <= 12)	return "AM";
	else
	{
		gdata.currentHrs -= 12;
		return "PM";
	}
}


//called over and over, use it to render things
void Render(float dTime, MyD3D& d3d, gamedata& gdata)
{
	WinUtil& wu = WinUtil::Get();
	d3d.BeginRender(Vector4(0,0,0,0));
	
	CommonStates dxstate(&d3d.GetDevice());
	gpSpriteBatch->Begin(SpriteSortMode_Deferred,dxstate.NonPremultiplied());

	Vector2 pos{ (float)wu.GetData().clientWidth / 2.f, (float)wu.GetData().clientHeight / 2.f };

	stringstream time, AMORPM, answer;
	time << setfill('0') << setw(2) << gdata.currentHrs << " : " << setfill('0') << setw(2) << gdata.currentMins << " : " <<  setfill('0') << setw(2) << gdata.currentSecs;
	AMORPM << gdata.amORpm;
	answer << "Which clock shows the same time as the digital one? 1, 2 or 3? " << ans;

	gpFont->DrawString(gpSpriteBatch, "Time to play?", Vector2(100,0));

	//Digital clock rendering
	gpSpriteBatch->Draw(gpDigital, Vector2(450, 150), nullptr, Vector4(1, 1, 1, 1), 0, Vector2(128, 128), Vector2(0.5, 0.5));
	gpFont->DrawString(gpSpriteBatch, time.str().c_str(), Vector2(430, 150), Vector4(1, 1, 1, 1), 0, Vector2(0,0), 1.5f);
	gpFont->DrawString(gpSpriteBatch, AMORPM.str().c_str(), Vector2(560, 190), Vector4(1, 1, 1, 1), 0, Vector2(0, 0), 1.f);

	//Analog clock rendering
	gpFont->DrawString(gpSpriteBatch, "1: ", Vector2(30, 300), Vector4(1, 1, 1, 1), 0, Vector2(0, 0), 1.5f);
	gpFont->DrawString(gpSpriteBatch, "2: ", Vector2(380, 300), Vector4(1, 1, 1, 1), 0, Vector2(0, 0), 1.5f);
	gpFont->DrawString(gpSpriteBatch, "3: ", Vector2(720, 300), Vector4(1, 1, 1, 1), 0, Vector2(0, 0), 1.5f);
	gpSpriteBatch->Draw(gpAnalog, Vector2(120, 380), nullptr, Vector4(1, 1, 1, 1), 0, Vector2(128, 128), Vector2(0.5, 0.5));
	gpSpriteBatch->Draw(gpAnalog, Vector2(480, 380), nullptr, Vector4(1, 1, 1, 1), 0, Vector2(128, 128), Vector2(0.5, 0.5));
	gpSpriteBatch->Draw(gpAnalog, Vector2(820, 380), nullptr, Vector4(1, 1, 1, 1), 0, Vector2(128, 128), Vector2(0.5, 0.5));

	//Clock Hands
	const RECT hourRect = { 7, 135, 90, 483};
	const RECT minRect = { 98, 0, 145, 483};

	gpSpriteBatch->Draw(gpClockHands, Vector2(180, 442), &(RECT)minRect, Vector4(1, 1, 1, 1), gdata.Angles[gdata.randNum][1], Vector2(25, 455), Vector2(0.25f, 0.25f));
	gpSpriteBatch->Draw(gpClockHands, Vector2(180, 442), &(RECT)hourRect, Vector4(1, 1, 1, 1), gdata.Angles[gdata.randNum][0], Vector2(47, 319), Vector2(0.25f, 0.25f));

	if (gdata.randNum == 0)	gdata.randNum2 = 2;
	else if (gdata.randNum == 1)	gdata.randNum2 = 0;
	else if (gdata.randNum == 2)	gdata.randNum2 = 1;

	gpSpriteBatch->Draw(gpClockHands, Vector2(545, 442), &(RECT)minRect, Vector4(1, 1, 1, 1), gdata.Angles[gdata.randNum2][1], Vector2(25, 455), Vector2(0.25f, 0.25f));
	gpSpriteBatch->Draw(gpClockHands, Vector2(545, 442), &(RECT)hourRect, Vector4(1, 1, 1, 1), gdata.Angles[gdata.randNum2][0], Vector2(47, 319), Vector2(0.25f, 0.25f));

	if (gdata.randNum2 == 0)	gdata.randNum3 = 2;
	else if (gdata.randNum2 == 1)	gdata.randNum3 = 0;
	else if (gdata.randNum2 == 2)	gdata.randNum3 = 1;

	gpSpriteBatch->Draw(gpClockHands, Vector2(885, 442), &(RECT)minRect, Vector4(1, 1, 1, 1), gdata.Angles[gdata.randNum3][1], Vector2(25, 455), Vector2(0.25f, 0.25f));
	gpSpriteBatch->Draw(gpClockHands, Vector2(885, 442), &(RECT)hourRect, Vector4(1, 1, 1, 1), gdata.Angles[gdata.randNum3][0], Vector2(47, 319), Vector2(0.25f, 0.25f));

	gpFont->DrawString(gpSpriteBatch, answer.str().c_str(), Vector2(100, 600));

	//Gets the clock with the correct answer
	if (gdata.randNum == 2)	gdata.correctAnswer = 1;
	else if(gdata.randNum2 == 2)	gdata.correctAnswer = 2;
	else if (gdata.randNum3 == 2)	gdata.correctAnswer = 3;

	string correctAns = to_string(gdata.correctAnswer);

	if (ans == correctAns)
		gpFont->DrawString(gpSpriteBatch, "Correct, Well Done!!!", Vector2(100, 650));
	else if(ans != correctAns && ans != "")
		gpFont->DrawString(gpSpriteBatch, "Incorrect, Try Again", Vector2(100, 650));

	gpSpriteBatch->End();
	d3d.EndRender();
}

//if ALT+ENTER or resize or drag window we might want do
//something like pause the game perhaps, but we definitely
//need to let D3D know what's happened (OnResize_Default).
void OnResize(int screenWidth, int screenHeight, MyD3D& d3d)
{
	d3d.OnResize_Default(screenWidth, screenHeight);
}

//messages come from windows all the time, should we respond to any specific ones?
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case 27:
		case 'q':
		case 'Q':
			PostQuitMessage(0);
			return 0;
		case VK_BACKSPACE:
			if (ans.length() > 0)
				ans.pop_back();
			break;
		case '1':
			ans = wParam;
			break;
		case '2':
			ans = wParam;
			break;
		case '3':
			ans = wParam;
			break;
		case 'r':
		case 'R':
			replay = true;
			ans = "";
		}
	}

	//default message handling (resize window, full screen, etc)
	return WinUtil::Get().DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

//main entry point for the game
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{

	int w(1024), h(768);
	if (!WinUtil::Get().InitMainWindow(w, h, hInstance, "Clocks", MainWndProc, true))
		assert(false);

	gamedata gdata;
	MyD3D d3d;
	if (!d3d.InitDirect3D(OnResize))
		assert(false);
	WinUtil::Get().SetD3D(d3d);
label:
	replay = false;
	InitGame(d3d, gdata);

	bool canUpdateRender;
	float dTime = 0;
	while (WinUtil::Get().BeginLoop(canUpdateRender))
	{
		if (canUpdateRender)
		{
			Update(dTime, d3d, gdata);
			Render(dTime, d3d, gdata);
			if (replay)	goto label;
		}
		dTime = WinUtil::Get().EndLoop(canUpdateRender);
	}

	ReleaseGame();
	d3d.ReleaseD3D(true);	
	return 0;
}